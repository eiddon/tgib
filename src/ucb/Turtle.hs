module Turtle 
( Turtle(..)
, moveForward
, moveBackward
, turnLeft
, turnRight
, turtle
, penDown
, penUp
, penErase
, penPaint
, penReverse
, setPenColour
, Colour(..)
, colourForInt
, difference
) where

import Data.Angle
import Geometry
import Picture
import Haste
import qualified Haste.Graphics.Canvas as G

data PenMode =
    PAINT |
    ERASE |
    REVERSE
    deriving Show

data PenPos =
    UP |
    DOWN
    deriving Show

data Colour = Colour Int Int Int deriving Show

data Pen = Pen PenPos PenMode Colour deriving Show

data Turtle = Turtle Vector Pen deriving Show

pen :: Pen
pen = Pen UP PAINT black

turtle :: Turtle
turtle = Turtle (Vector (Point 0 0) (Degrees 0)) pen

render :: Pen -> Vector -> Vector -> G.Picture()
render _ (Vector (Point x y) _) (Vector (Point x1 y1) _) = G.stroke $ G.line (x, y) (x1, y1)

p4T :: Turtle -> Point
p4T (Turtle (Vector p _) _) = p

difference :: Turtle -> Turtle -> Picture Point
difference (Turtle _ (Pen UP _ _)) _ = Pic []
difference t u = Pic [(p4T t, p4T u)]

moveForward :: Turtle -> Int -> Turtle
moveForward (Turtle v p) u = (Turtle (forward v u) p)

moveBackward :: Turtle -> Int -> Turtle
moveBackward (Turtle v p) u = (Turtle (backward v u) p)

turnRight :: Turtle -> Double -> Turtle
turnRight (Turtle v p) u = (Turtle (turn v u) p)

turnLeft :: Turtle -> Double -> Turtle
turnLeft (Turtle v p) u = (Turtle (turn v ((-1) * u)) p)

penDown :: Turtle -> Turtle
penDown (Turtle v (Pen _ m c)) = (Turtle v (Pen DOWN m c))

penUp :: Turtle -> Turtle
penUp (Turtle v (Pen _ m c)) = (Turtle v (Pen UP m c))

penErase :: Turtle -> Turtle
penErase (Turtle v (Pen p _ c)) = (Turtle v (Pen p ERASE c))

penPaint :: Turtle -> Turtle
penPaint (Turtle v (Pen p _ c)) = (Turtle v (Pen p PAINT c))

penReverse :: Turtle -> Turtle
penReverse (Turtle v (Pen p _ c)) = (Turtle v (Pen p REVERSE c))

setPenColourC :: Turtle -> Colour -> Turtle
setPenColourC (Turtle v (Pen p m _)) c = (Turtle v (Pen p m c))

setPenColour :: Turtle -> Int -> Turtle
setPenColour t c = setPenColourC t (colourForInt c)

black :: Colour
black = Colour 0 0 0

blue :: Colour
blue = Colour 0 0 100

green :: Colour
green = Colour 0 100 0

cyan :: Colour
cyan = Colour 0 100 100

red :: Colour
red = Colour 100 0 0

magenta :: Colour
magenta = Colour 100 0 100

yellow :: Colour
yellow = Colour 100 100 0

white :: Colour
white = Colour 100 100 100

colourForInt :: Int -> Colour
colourForInt 0 = black
colourForInt 1 = blue
colourForInt 2 = green
colourForInt 3 = cyan
colourForInt 4 = red
colourForInt 5 = magenta
colourForInt 6 = yellow
colourForInt 7 = white
colourForInt _ = black --should conform to user settable colours

