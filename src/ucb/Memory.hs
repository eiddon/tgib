{-# LANGUAGE ExistentialQuantification #-}
module Memory
( Memory(Mem)
, put
, get
)
where
data Memory k v =
    (Show k, Show v, Eq k) => Mem [(k, v)]

instance Show (Memory k v) where
    show (Mem kvs) = show kvs

put :: Memory k v -> (k, v) -> Memory k v
put (Mem kvs) kv = Mem (kv:kvs)

get :: Memory k v -> k -> Maybe v
get (Mem []) _ = Nothing
get (Mem ((k,v):kvs)) k1 | k == k1 = Just v
                         | otherwise = get (Mem kvs) k1
