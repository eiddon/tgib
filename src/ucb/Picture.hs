{-# LANGUAGE GeneralizedNewtypeDeriving, DeriveFunctor #-}
module Picture
( Picture(..)
, paste
)
where

import Data.Monoid

newtype Picture p = Pic [(p,p)] deriving (Functor, Monoid, Show)

paste :: Picture p -> Picture p -> Picture p
paste (Pic ps) (Pic qs) = Pic (ps ++ qs)

