{-# LANGUAGE PackageImports #-}
import Text.ParserCombinators.Parsec hiding (State)
import Control.Applicative hiding ((<|>), many, optional) -- http://book.realworldhaskell.org/read/using-parsec.html#id652399
import Control.Monad hiding (join)
import "mtl" Control.Monad.State
import Parse
import Turtle
import Data.Angle
import qualified Data.Map as Map
import Picture
import Geometry
import Memory

type M a = State (Memory String a)

data TDir =
    FD |
    BW |
    LEFT |
    RIGHT
    deriving Show

data TPen =
    PENDOWN |
    PENUP |
    PENPAINT |
    PENERASE |
    PENREVERSE |
    SETPENCOLOR Int
    deriving Show

data PExpr =
    EQUALP NExpr NExpr |
    LESSP NExpr NExpr |
    GREATERP NExpr NExpr |
    LESEQUALSP NExpr NExpr | 
    GREATEREQUALP NExpr NExpr |
    AND PExpr PExpr |
    OR PExpr PExpr |
    NOT PExpr 
    deriving Show

data NExpr =
    HEADING |
    TOWARDS LExpr |
    XCOR |
    YCOR |
    SUM NExpr NExpr [NExpr] |
    DIFFERENCE NExpr NExpr |
    MINUS NExpr |
    PRODUCT NExpr NExpr [NExpr] 
    deriving Show
    --TODO:QUOTIENT, REMAINDER, MODULO

data LExpr =
    POS |
    SCRUNCH |
    LIST [Expr] |
    ISEQ NExpr NExpr
    deriving Show
    --TODO:RSEQ

data WExpr =
    WORD WExpr WExpr [WExpr]
    deriving Show

data Expr =
    WExpr |
    LExpr |
    NExpr
    deriving Show

--Essentially VExpr but deserves its own status as the only type of Expr you can just execute
data TCommand =
    TMove TDir Int |
    TPen TPen
    deriving Show
    
turtleProgram = sepBy1 command whitespace

command = try move <|> pen <?> "command"

whitespace = try eol <|> many1 (char ' ') <?> "whitespace"

dirForString :: String -> TDir
dirForString "FORWARD" = FD
dirForString "FD" = FD
dirForString "BACKWARD" = BW
dirForString "BW" = BW
dirForString "LEFT" = LEFT
dirForString "LT"   = LEFT
dirForString "RIGHT" = RIGHT
dirForString "RT"    = RIGHT
dirForString u = error $ "undefined direction for: " ++ u

--TODO: Understand the monad hiding of do.
move :: GenParser Char st TCommand
move = do
       dir <- dirForString <$> (search ("FD" : "FORWARD" : "BW" : "BACKWARD" : "LEFT" : "LT" : "RIGHT" : "RT" : []) "direction")
       whitespace
       quant <- number
       return (TMove dir quant)

penForString :: String -> TPen
penForString "PENDOWN" = PENDOWN
penForString "PD" = PENDOWN
penForString "PENUP" = PENUP
penForString "PU" = PENUP
penForString "PENPAINT" = PENPAINT
penForString "PPT" = PENPAINT
penForString "PENERASE" = PENERASE
penForString "PE" = PENERASE
penForString "PENREVERSE" = PENREVERSE
penForString "PX" = PENREVERSE
penForString s = error $ "undfined pen command for: " ++ s


pen :: GenParser Char st TCommand
pen = penParam <|>
      do
      pen <- penForString <$> (search ("PENDOWN" : "PD" : "PENUP" : "PU" : "PENPAINT" : "PPT" : "PENERASE" : "PE" : "PENREVERSE" : "PX" : []) "pen command")
      return (TPen pen)

penParam :: GenParser Char st TCommand
penParam = do
           search ("SETPENCOLOR" : "SETPC" : []) "SETPC or SETPENCOLOR"
           whitespace
           quant <- number
           return (TPen (SETPENCOLOR quant))

-- http://book.realworldhaskell.org/read/using-parsec.html
eol =   try (string "\n\r")
        <|> try (string "\r\n")
        <|> string "\n"
        <|> string "\r"
        <?> "end of line"

parseMove :: String -> Either ParseError TCommand
parseMove = parse move "(unknown)"

parsePen :: String -> Either ParseError TCommand
parsePen = parse pen "(unknown)"

parseCommand :: String -> Either ParseError TCommand
parseCommand = parse command "(unknown)"

parseTProgram :: String -> Either ParseError [TCommand]
parseTProgram = parse turtleProgram "(unknown)"

evalMove :: TCommand -> Turtle -> Turtle
evalMove (TMove FD u) t = moveForward t u
evalMove (TMove BW u) t = moveBackward t u
evalMove (TMove LEFT u) t = turnLeft t (fromIntegral u)
evalMove (TMove RIGHT u) t = turnRight t (fromIntegral u)
evalMove c _ = error $ "Not a TMove: " ++ (show c)

evalPen :: TCommand -> Turtle -> Turtle
evalPen (TPen PENDOWN) t = penDown t
evalPen (TPen PENUP) t = penUp t
evalPen (TPen PENPAINT) t = penPaint t
evalPen (TPen PENERASE) t = penErase t
evalPen (TPen PENREVERSE) t = penReverse t
evalPen (TPen (SETPENCOLOR c)) t = setPenColour t c
evalPen c _ = error $ "Not a TPen: " ++ (show c)

--Hide this in a monad
eval :: TCommand -> Turtle -> Turtle
eval (TMove dir units) t = evalMove (TMove dir units) t
eval (TPen pen) t = evalPen (TPen pen) t

exec :: Turtle -> [TCommand] -> (Turtle, Picture Point)
exec t [] = (t, Pic [])
exec t (c:cs) = (fst res, paste (difference t next) (snd res))
                where next = eval c t
                      res = exec next cs

sexec :: Turtle -> Either ParseError [TCommand] -> String
sexec t = either die (svg . snd . exec t)

die :: ParseError -> String
die = show

--TODO: SVG supports relative moves, these co-ords are absolute. This is OK but might not be the best option.
segment :: (Point, Point) -> String
segment ((Point x1 y1), (Point x2 y2)) = "M " ++ (show x1) ++ " " ++ (show y1) ++ " L " ++ (show x2) ++ " " ++ (show y2)

svg :: Picture Point -> String
svg (Pic ps) = "<svg><path d='" ++ (foldl (++) [] (map (((flip (++)) " ") . segment) ps)) ++ "z' stroke='black' stroke-width='1'/></svg>"





