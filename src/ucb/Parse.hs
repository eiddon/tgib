module Parse
( parseInt
, number
, search
) where

import Text.ParserCombinators.Parsec
import Control.Applicative hiding ((<|>), many, optional)

parseInt :: Int -> String -> Int
parseInt n []       = n
parseInt n ('0':ds) = parseInt (n * 10) ds
parseInt n ('1':ds) = parseInt (n * 10 + 1) ds
parseInt n ('2':ds) = parseInt (n * 10 + 2) ds
parseInt n ('3':ds) = parseInt (n * 10 + 3) ds
parseInt n ('4':ds) = parseInt (n * 10 + 4) ds
parseInt n ('5':ds) = parseInt (n * 10 + 5) ds
parseInt n ('6':ds) = parseInt (n * 10 + 6) ds
parseInt n ('7':ds) = parseInt (n * 10 + 7) ds
parseInt n ('8':ds) = parseInt (n * 10 + 8) ds
parseInt n ('9':ds) = parseInt (n * 10 + 9) ds
parseInt _ (c:_)    = error $ "Unexpected character: " ++ [c]

number :: GenParser Char st Int
number = do
         quant <- parseInt 0 <$> ((many1 digit) <?> "number")
         return quant

search []     _ = error "can't compose empty list"
search (s:[]) e = try (string s) <?> e
search (s:ss) e = try (string s) <|> (search ss e)
