module Geometry
( Point(..)
, Vector(..)
, forward
, backward
, turn
) where

import Data.Angle

{--
type Point  = (Double, Double)
type Vector = (Point, Degrees Double)
--}

data Point = Point Double Double deriving Show
data Vector = Vector Point (Degrees Double) deriving Show

forward :: Vector -> Int -> Vector
forward (Vector (Point x y) a) u = Vector (Point (x + u1) (y + u2)) a
                             where
                             u1 = (fromIntegral u) * cosine a
                             u2 = (fromIntegral u) * sine a

backward :: Vector -> Int -> Vector
backward (Vector (Point x y) a) u = Vector (Point (x - u1) (y - u2)) a
                                  where
                                  u1 = (fromIntegral u) * cosine a
                                  u2 = (fromIntegral u) * sine a

turn :: Vector -> Double -> Vector
turn (Vector point (Degrees a)) d = Vector point (Degrees (a + d))
